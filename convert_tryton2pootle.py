#!/usr/bin/env python
# The COPYRIGHT file at the top level of this repository contains the full
# content notices and license terms.
import os
from optparse import OptionParser

usage = '''%prog [options]

This script is intended to transform a set of po files
exported from Tryton into the required import format by pootle.

TODO:
    - Make this a trytond patch
'''

revision = 1

def main(src_path, dest_path, lang):
    for po_file in os.listdir(src_path):
        repos_name, ext = os.path.splitext(po_file)
        if (po_file.startswith('.') or ext != '.po'):
            continue
        print(po_file)
        content = '''msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\\n"
"Report-Msgid-Bugs-To: \\n"
"POT-Creation-Date: 2016-04-11 10:36+0000\\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\\n"
"Last-Translator: Mathias Behrle <mbehrle@m9s.biz>\\n"
"Language-Team: LANGUAGE <LL@li.org>\\n"
"Language: de_DE\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
"X-Generator: Translate Toolkit 1.13.0\\n"
"X-Pootle-Path: /de_DE/tryton/%s\\n"
"X-Pootle-Revision: %s\\n"
''' % (po_file, revision)

        with open(po_file) as fp:
            # omit the first three lines from Tryton
            i = 0
            for line in fp:
                if i < 3:
                    i += 1
                    continue
                content += line

        with open(po_file, 'w') as fp:
            fp.write(content)



if __name__ == '__main__':
    parser = OptionParser(usage)
    parser.add_option("-s", "--src", dest="src_path",
                  help="The path to the directory containing the po files")
    opts, args = parser.parse_args()

    main(opts.src_path, opts.dest_path, opts.lang)
